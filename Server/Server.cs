﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Server
{
    class Server : IDisposable
    {
        public static List<ZodiacSign> signs;

        public Grpc.Core.Server GrpcServer { get; private set; }

        public Action CloseServerAction { get; set; }

        public IEnumerable<Grpc.Core.ServerServiceDefinition> Services
        {
            get
            {
                yield return Generated.WinterZodiacSignService.BindService(new WinterZodiacSignOperationService());
                yield return Generated.SpringZodiacSignService.BindService(new SpringZodiacSignOperationService());
                yield return Generated.SummerZodiacSignService.BindService(new SummerZodiacSignOperationService());
                yield return Generated.FallZodiacSignService.BindService(new FallZodiacSignOperationService());
            }
        }

        public Server(string host, int port)
        {
            signs = new List<ZodiacSign>();

            ReadSignsFromFile("..\\..\\intervals.txt");

            GrpcServer = new Grpc.Core.Server()
            {
                Ports = { new Grpc.Core.ServerPort(host, port, Grpc.Core.ServerCredentials.Insecure) }
            };

            LoadServices();
        }

        public void Start()
        {
            GrpcServer.Start();

            Console.WriteLine(string.Format("Server started ({0}:{1}).", Configuration.HOST, Configuration.PORT));
        }

        private void LoadServices()
        {
            Services.ToList().ForEach(service => GrpcServer.Services.Add(service));
        }

        public void Dispose()
        {
            CloseServerAction.Invoke();
            GrpcServer.ShutdownAsync().Wait();
            var port = GrpcServer.Ports.FirstOrDefault();
            Console.WriteLine("Server closed ({0}:{1}).", Configuration.HOST, Configuration.PORT);
        }

        private void ReadSignsFromFile(string path)
        {
            string[] lines = System.IO.File.ReadAllLines(path);

            foreach (string line in lines)
            {
                string[] temp = line.Split();

                string name = temp[0];
                int beginMonth = (temp[1][0] - 48) * 10 + temp[1][1] - 48;
                int beginDay = (temp[2][0] - 48) * 10 + temp[2][1] - 48;

                int endMonth = (temp[4][0] - 48) * 10 + temp[4][1] - 48;
                int endDay = (temp[5][0] - 48) * 10 + temp[5][1] - 48;

                signs.Add(new ZodiacSign(name,new ZodiacSign.Date(beginMonth, beginDay),new ZodiacSign.Date(endMonth, endDay)));
            }
        }
    }
}

﻿using Grpc.Core;
using System;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            const string Host = "localhost";
            const int Port = 16842;

            var channel = new Channel($"{Host}:{Port}", ChannelCredentials.Insecure);

            string date;

            do
            {
                Console.Write("Date (MM/DD/YYYY): ");
                date = Console.ReadLine();
            } while (!Validate(date));


            int month = (date[0] - 48) * 10 + date[1] - 48;

            switch (month/3)
            {
                case 4:
                case 0:
                    {
                        var client = new Generated.WinterZodiacSignService.WinterZodiacSignServiceClient(channel);

                        var response = client.GetZodiacSign(new Generated.WinterZodiacSignRequest
                        {
                            Date = date
                        });

                        Console.WriteLine("\nResponse: {0}", response.ZodiacSign);

                        break;
                    }
                case 1:
                    {
                        var client = new Generated.SpringZodiacSignService.SpringZodiacSignServiceClient(channel);

                        var response = client.GetZodiacSign(new Generated.SpringZodiacSignRequest
                        {
                            Date = date
                        });

                        Console.WriteLine("\nResponse: {0}", response.ZodiacSign);

                        break;
                    }
                case 2:
                    {
                        var client = new Generated.SummerZodiacSignService.SummerZodiacSignServiceClient(channel);

                        var response = client.GetZodiacSign(new Generated.SummerZodiacSignRequest
                        {
                            Date = date
                        });

                        Console.WriteLine("\nResponse: {0}", response.ZodiacSign);

                        break;
                    }
                case 3:
                    {
                        var client = new Generated.FallZodiacSignService.FallZodiacSignServiceClient(channel);

                        var response = client.GetZodiacSign(new Generated.FallZodiacSignRequest
                        {
                            Date = date
                        });

                        Console.WriteLine("\nResponse: {0}", response.ZodiacSign);

                        break;
                    }
            }


            // Shutdown
            channel.ShutdownAsync().Wait();
        }

        private static bool Validate(string date)
        {
            // sintax

            if (date.Length != 10)
            {
                Console.WriteLine("Invalid format.");
                return false;
            }

            if (date[2] != '/' || date[5] != '/')
            {
                Console.WriteLine("Invalid format.");
                return false;
            }

            if (!(Char.IsDigit(date[0]) && Char.IsDigit(date[1]) &&
                Char.IsDigit(date[3]) && Char.IsDigit(date[4]) &&
                Char.IsDigit(date[6]) && Char.IsDigit(date[7]) && Char.IsDigit(date[8]) && Char.IsDigit(date[9])))
            {
                Console.WriteLine("Invalid numbers.");
                return false;
            }


            //// actual numbers numbers

            int month = (date[0] - 48) * 10 + date[1] - 48;
            int day = (date[3] - 48) * 10 + date[4] - 48;
            int year = (date[6] - 48) * 1000 + (date[7] - 48) * 100 + (date[8] - 48) * 10 + date[9] - 48;

            // month & day

            if (month > 12 || month < 1)
            {
                Console.WriteLine("Invalid month.");
                return false;
            }

            if (day < 1)
            {
                Console.WriteLine("Invalid day.");
                return false;
            }

            if (month < 8)
            {
                switch (month % 2)  // the months on the "left hand"
                {
                    case 0:
                        {
                            if (month == 2)    // february
                            {
                                if (day > 29)
                                {
                                    Console.WriteLine("February can't have more than 29 days.");
                                    return false;
                                }
                            }
                            else
                            {
                                if (day > 30)    // the even months
                                {
                                    Console.WriteLine("This month can't have more than 30 days.");
                                    return false;
                                }
                            }
                            break;
                        }
                    case 1:
                        {
                            if (day > 31)   // the odd months
                            {
                                Console.WriteLine("This month can't have more than 31 days.");
                                return false;
                            }
                            break;
                        }
                }
            }
            else
            {
                switch (month % 2)  // the months on the "right hand"
                {
                    case 0:
                        {
                            if (day > 31)    // the even months
                            {
                                Console.WriteLine("This month can't have more than 31 days.");
                                return false;
                            }

                            break;
                        }
                    case 1:
                        {
                            if (day > 30)   // the odd months
                            {
                                Console.WriteLine("This month can't have more than 30 days.");
                                return false;
                            }
                            break;
                        }
                }
            }


            // year

            if (month == 2 && day == 29)
                if (year % 4 != 0)
                {
                    Console.WriteLine($"{year} is not a bisect year.");
                    return false;
                }

            return true;
        }
    }
}
